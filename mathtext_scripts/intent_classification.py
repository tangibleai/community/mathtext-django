import joblib
import numpy as np
from sentence_transformers import SentenceTransformer
from core.constants import MODEL_PATH


encoder = SentenceTransformer('all-MiniLM-L6-v2')
model = joblib.load(MODEL_PATH)


def predict_message_intent(message):
    tokenized_utterance = np.array([list(encoder.encode(message))])
    predicted_label = model.predict(tokenized_utterance)
    predicted_probabilities = model.predict_proba(tokenized_utterance)
    confidence_score = predicted_probabilities.max()

    return {
        "type": "intent",
        "data": predicted_label[0],
        "confidence": confidence_score
    }